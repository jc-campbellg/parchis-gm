function GmEvent(type, data) {
  const event = new CustomEvent('gmEvent', {
    detail: {
      type: type,
      data: data
    }
  }); 

  window.dispatchEvent(event);
};