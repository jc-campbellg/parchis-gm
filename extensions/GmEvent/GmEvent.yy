{
    "id": "8b5874a0-88e1-43fa-accc-d853b093d0e9",
    "modelName": "GMExtension",
    "mvc": "1.2",
    "name": "GmEvent",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        
    ],
    "androidProps": false,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 32,
    "date": "2020-23-15 03:04:34",
    "description": "",
    "exportToGame": true,
    "extensionName": "",
    "files": [
        {
            "id": "ce5559a5-5a3f-4a9f-b585-a9d6bdf43a70",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 32,
            "filename": "GmEvent.js",
            "final": "",
            "functions": [
                {
                    "id": "57135c9f-3ddc-4f4f-a5d0-4c66f00e9049",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1,
                        2
                    ],
                    "externalName": "GmEvent",
                    "help": "GmEvent(type, data);",
                    "hidden": false,
                    "kind": 5,
                    "name": "GmEvent",
                    "returnType": 1
                }
            ],
            "init": "",
            "kind": 5,
            "order": [
                "57135c9f-3ddc-4f4f-a5d0-4c66f00e9049"
            ],
            "origname": "",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": false,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosdelegatename": "",
    "iosplistinject": "",
    "license": "",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "options": null,
    "optionsFile": "options.json",
    "packageID": "",
    "productID": "",
    "sourcedir": "",
    "supportedTargets": -1,
    "tvosProps": false,
    "tvosSystemFrameworkEntries": [
        
    ],
    "tvosThirdPartyFrameworkEntries": [
        
    ],
    "tvosclassname": "",
    "tvosdelegatename": "",
    "tvosmaccompilerflags": "",
    "tvosmaclinkerflags": "",
    "tvosplistinject": "",
    "version": "0.0.1"
}