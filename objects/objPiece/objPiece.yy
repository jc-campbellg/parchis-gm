{
    "id": "87c95e8b-ddf6-460c-9584-a83c137031c4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objPiece",
    "eventList": [
        {
            "id": "f59579d5-bc91-48b4-bbf6-9f3f16c9618e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "87c95e8b-ddf6-460c-9584-a83c137031c4"
        },
        {
            "id": "2ad1c5af-16a7-46f6-b3cf-ad401a47db67",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 7,
            "m_owner": "87c95e8b-ddf6-460c-9584-a83c137031c4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "2f05c8b0-6f72-42e3-a2b6-09bf0179cd38",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "image_speed",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "adc848b5-29b6-42ea-8180-bd66deadf1c1",
    "visible": true
}