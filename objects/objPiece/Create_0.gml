/// @description Set sprite_index
myNumber = get_instance_number(objPiece);
switch (myNumber) {
    case 0:
	case 1:
	case 2:
	case 3:
        sprite_index = sprRedPiece;
		myColor = gameColor.red;
        break;
	case 4:
	case 5:
	case 6:
	case 7:
        sprite_index = sprBluePiece;
		myColor = gameColor.blue;
        break;
	case 8:
	case 9:
	case 10:
	case 11:
        sprite_index = sprYellowPiece;
		myColor = gameColor.yellow;
        break;
	case 12:
	case 13:
	case 14:
	case 15:
        sprite_index = sprGreenPiece;
		myColor = gameColor.green;
        break;
	case 16:
	case 17:
	case 18:
	case 19:
        sprite_index = sprPurplePiece;
		myColor = gameColor.purple;
        break;
	case 20:
	case 21:
	case 22:
	case 23:
        sprite_index = sprOrangePiece;
		myColor = gameColor.orange;
        break;
}