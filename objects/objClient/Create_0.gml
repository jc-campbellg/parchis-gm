/// @description Connect to server
client = network_create_socket(network_socket_ws);
isOnline = network_connect(client, "parchis.disenadospordios.com", 6510);
clientBuffer = buffer_create(2048, buffer_fixed, 1);

if (isOnline < 0) {
	show_message("No pude conectarme al servidor.");
}

send_sync_positions();