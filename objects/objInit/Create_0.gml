enum net {
	syncNames,
	tryAgainColor,
	join,
	newJoin,
	removePlayer,
	jumpToPosition,
	syncPositions,
	moveToPosition,
	ready,
	gameStart,
	chat
};

enum gameColor {
	red,
	blue,
	yellow,
	green,
	purple,
	orange
};