var color = argument0;
var client = objClient.client;
var clientBuffer = objClient.clientBuffer;

buffer_seek(clientBuffer, buffer_seek_start, 0);
buffer_write(clientBuffer, buffer_u8, net.ready);
buffer_write(clientBuffer, buffer_u8, color);
network_send_packet(client, clientBuffer, buffer_tell(clientBuffer));