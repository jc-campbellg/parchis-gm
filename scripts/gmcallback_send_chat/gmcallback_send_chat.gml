var message = argument0;
var client = objClient.client;
var clientBuffer = objClient.clientBuffer;

buffer_seek(clientBuffer, buffer_seek_start, 0);
buffer_write(clientBuffer, buffer_u8, net.chat);
buffer_write(clientBuffer, buffer_string, message);
network_send_packet(client, clientBuffer, buffer_tell(clientBuffer));