var name = argument0;
var color = argument1;
var client = objClient.client;
var clientBuffer = objClient.clientBuffer;

buffer_seek(clientBuffer, buffer_seek_start, 0);
buffer_write(clientBuffer, buffer_u8, net.join);
buffer_write(clientBuffer, buffer_string, name);
buffer_write(clientBuffer, buffer_u8, color);
network_send_packet(client, clientBuffer, buffer_tell(clientBuffer));