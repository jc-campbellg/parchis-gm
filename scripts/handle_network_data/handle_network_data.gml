var buffer = argument0;
var msgId = buffer_read(buffer, buffer_u8);

switch(msgId) {
	case net.syncNames:
		var players = [];
		for (var i = 0; i < 6; ++i) {
		    var data = ds_map_create();
			ds_map_add(data, "available", buffer_read(buffer, buffer_bool));
			ds_map_add(data, "name", buffer_read(buffer, buffer_string));
			ds_map_add(data, "ready", buffer_read(buffer, buffer_bool));
			players[i] = json_encode(data);
			ds_map_destroy(data);
		}
		GmEvent("syncNames", players);
		break;
		
	case net.join:
		GmEvent("join", players);
		break;
		
	case net.tryAgainColor:
		GmEvent("tryAgainColor", true);
		break;
		
	case net.ready:
		var players = [];
		var color = buffer_read(buffer, buffer_u8);
		GmEvent("ready", color);
		break;
		
	case net.gameStart:
		GmEvent("gameStart", true);
		break;
		
	case net.newJoin:
		var data = ds_map_create();
		ds_map_add(data, "name", buffer_read(buffer, buffer_string));
		ds_map_add(data, "color", buffer_read(buffer, buffer_u8));
		var dataJson = json_encode(data);
		ds_map_destroy(data);
		GmEvent("newJoin", dataJson);
		break;
		
	case net.removePlayer:
		var c = buffer_read(buffer, buffer_u8)
		GmEvent("removePlayer", c);
		break;
		
	case net.jumpToPosition:
		var num = buffer_read(buffer, buffer_u8);
		var xx = buffer_read(buffer, buffer_u16);
		var yy = buffer_read(buffer, buffer_u16);
		
		var inst = instance_find(objPiece, num);
		with (inst) {
			x = xx;
			y = yy;
		};
		break;
		
	case net.moveToPosition:
		var movePathTemp = path_add();
		var num = buffer_read(buffer, buffer_u8);
		var steps = buffer_read(buffer, buffer_u8);
		repeat(steps) {
			var px = buffer_read(buffer, buffer_u16);
			var py = buffer_read(buffer, buffer_u16);
			path_add_point(movePathTemp, px, py, 20);
		}
		path_set_closed(movePathTemp, false);
		
		with (instance_find(objPiece, num)) {
			movePath = movePathTemp;
			path_start(movePath, 20, path_action_stop, true);
		}
		break;
		
	case net.chat:
		var msg = buffer_read(buffer, buffer_string);
		GmEvent("chat", msg);
		break;
}