/// @function get_instance_number(obj)
/// @description Return creation number of instance
/// @param obj Object to check for

var n = 0;

while (instance_find(argument0, n) != id) {
    n = n + 1;
}

return n;