{
    "id": "880a8817-1c1b-42cd-9f99-183243912c4b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprOrangePiece",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "711290f2-bea8-49a5-bec0-a6e54a710154",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "880a8817-1c1b-42cd-9f99-183243912c4b",
            "compositeImage": {
                "id": "8934679a-8b49-426b-bedc-5ac7f6eef619",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "711290f2-bea8-49a5-bec0-a6e54a710154",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cf36aae-2420-4981-a3a0-c0c27af93195",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "711290f2-bea8-49a5-bec0-a6e54a710154",
                    "LayerId": "8a819812-7a5b-4849-aad4-977b330766b1"
                }
            ]
        },
        {
            "id": "86fd5e66-ab4a-4ca4-a0a7-944bc19c2ed4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "880a8817-1c1b-42cd-9f99-183243912c4b",
            "compositeImage": {
                "id": "c50147c2-bb82-4c31-aa27-49bcd219b682",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86fd5e66-ab4a-4ca4-a0a7-944bc19c2ed4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bcfd831-b501-482a-89b4-059b0c419a58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86fd5e66-ab4a-4ca4-a0a7-944bc19c2ed4",
                    "LayerId": "8a819812-7a5b-4849-aad4-977b330766b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "8a819812-7a5b-4849-aad4-977b330766b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "880a8817-1c1b-42cd-9f99-183243912c4b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 9,
    "yorig": 26
}