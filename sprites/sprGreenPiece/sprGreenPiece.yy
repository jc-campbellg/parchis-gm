{
    "id": "9b425939-3a74-45ed-b895-d5e633c836ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGreenPiece",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 18,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "be63e431-dac7-4278-a3ba-0ddf51483e0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b425939-3a74-45ed-b895-d5e633c836ec",
            "compositeImage": {
                "id": "699ca93d-9b69-4fff-9f69-c30945528ed0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be63e431-dac7-4278-a3ba-0ddf51483e0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "324dddf3-e6e3-467a-8889-ee045ded6494",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be63e431-dac7-4278-a3ba-0ddf51483e0d",
                    "LayerId": "8b1695d1-5576-406e-afda-ac77eecddf37"
                }
            ]
        },
        {
            "id": "7c31d856-b9a9-442b-8e33-985008a752cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b425939-3a74-45ed-b895-d5e633c836ec",
            "compositeImage": {
                "id": "4b1a12f1-5e18-4053-bac4-ec646b210d54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c31d856-b9a9-442b-8e33-985008a752cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c1d7b24-cc71-4013-8b39-e51bc7a90fa9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c31d856-b9a9-442b-8e33-985008a752cd",
                    "LayerId": "8b1695d1-5576-406e-afda-ac77eecddf37"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "8b1695d1-5576-406e-afda-ac77eecddf37",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b425939-3a74-45ed-b895-d5e633c836ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 9,
    "yorig": 26
}