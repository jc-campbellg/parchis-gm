{
    "id": "7a19813f-8563-4ec6-8bcb-c7ef2957d621",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprWood",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 399,
    "bbox_left": 0,
    "bbox_right": 399,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e014f8a-15fd-4a7e-aca3-173ecf9e884e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a19813f-8563-4ec6-8bcb-c7ef2957d621",
            "compositeImage": {
                "id": "473ce264-3201-4b9d-bb51-f841c2b76286",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e014f8a-15fd-4a7e-aca3-173ecf9e884e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f86dbfdf-d056-4118-b979-50c55926732c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e014f8a-15fd-4a7e-aca3-173ecf9e884e",
                    "LayerId": "3f64c618-50f2-4d50-9ac9-ba844991d90b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "3f64c618-50f2-4d50-9ac9-ba844991d90b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a19813f-8563-4ec6-8bcb-c7ef2957d621",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": 0,
    "yorig": 0
}