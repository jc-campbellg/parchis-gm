{
    "id": "f485a139-467b-4b40-be4f-c1589037d355",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBoard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 600,
    "bbox_left": 0,
    "bbox_right": 590,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d4f42016-da82-4b57-81a0-7d9ff2fcc394",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f485a139-467b-4b40-be4f-c1589037d355",
            "compositeImage": {
                "id": "6a0a7f69-30f9-4af4-93c9-edb20587733f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4f42016-da82-4b57-81a0-7d9ff2fcc394",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a5df7af-b575-4942-9871-4f309bf4d768",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4f42016-da82-4b57-81a0-7d9ff2fcc394",
                    "LayerId": "770e9f47-7ef0-49e9-a7f5-02885bbfab5a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 601,
    "layers": [
        {
            "id": "770e9f47-7ef0-49e9-a7f5-02885bbfab5a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f485a139-467b-4b40-be4f-c1589037d355",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 591,
    "xorig": 0,
    "yorig": 0
}