{
    "id": "c73c5242-f538-4c86-b3b0-a4ae86966ba4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPurplePiece",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 18,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "601f3278-8298-4ed5-9224-547741a786bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c73c5242-f538-4c86-b3b0-a4ae86966ba4",
            "compositeImage": {
                "id": "77c19d06-3427-4b29-9aca-de95e4ac0df6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "601f3278-8298-4ed5-9224-547741a786bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e169c19d-0583-48df-a337-eea9b205f884",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "601f3278-8298-4ed5-9224-547741a786bb",
                    "LayerId": "a52015e5-2c90-46c1-b034-e97b48f028cb"
                }
            ]
        },
        {
            "id": "356aef17-7d67-49ea-b488-b97a39a1c92a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c73c5242-f538-4c86-b3b0-a4ae86966ba4",
            "compositeImage": {
                "id": "1e60af77-71d5-4f5a-9b29-69264ba7ea39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "356aef17-7d67-49ea-b488-b97a39a1c92a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "317a36bb-3a56-44e3-b4ed-c29f4b50b7e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "356aef17-7d67-49ea-b488-b97a39a1c92a",
                    "LayerId": "a52015e5-2c90-46c1-b034-e97b48f028cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "a52015e5-2c90-46c1-b034-e97b48f028cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c73c5242-f538-4c86-b3b0-a4ae86966ba4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 9,
    "yorig": 26
}