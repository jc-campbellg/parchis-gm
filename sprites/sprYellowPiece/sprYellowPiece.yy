{
    "id": "5cec1c4d-2101-46b6-b5dc-065d68e83c67",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprYellowPiece",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b720794f-18c5-4324-ba4e-bbb1a2a06f5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cec1c4d-2101-46b6-b5dc-065d68e83c67",
            "compositeImage": {
                "id": "9f2737ae-4a95-4fbb-a90b-1b0775b5d149",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b720794f-18c5-4324-ba4e-bbb1a2a06f5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2057cae-a647-41d4-a209-91625d261ed4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b720794f-18c5-4324-ba4e-bbb1a2a06f5b",
                    "LayerId": "21c211af-93ac-487e-8c7a-5ba1e93c67bf"
                }
            ]
        },
        {
            "id": "e824ba3b-9a5b-4474-994b-1e189cc3acea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cec1c4d-2101-46b6-b5dc-065d68e83c67",
            "compositeImage": {
                "id": "d08e977c-e305-487d-8c14-fe636fb22411",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e824ba3b-9a5b-4474-994b-1e189cc3acea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df29b3ba-608c-41b3-b017-3461129c30d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e824ba3b-9a5b-4474-994b-1e189cc3acea",
                    "LayerId": "21c211af-93ac-487e-8c7a-5ba1e93c67bf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "21c211af-93ac-487e-8c7a-5ba1e93c67bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5cec1c4d-2101-46b6-b5dc-065d68e83c67",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 9,
    "yorig": 26
}