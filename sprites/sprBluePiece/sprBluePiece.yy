{
    "id": "35228085-dd2e-4625-9792-3beb4cead961",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBluePiece",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 18,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c2ef1ad-03fc-4377-843d-91f062a123f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35228085-dd2e-4625-9792-3beb4cead961",
            "compositeImage": {
                "id": "a8b498f3-f9bf-42b0-90b2-f585059ac969",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c2ef1ad-03fc-4377-843d-91f062a123f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f99b0cd-0220-46d4-b719-3ae4ccea8612",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c2ef1ad-03fc-4377-843d-91f062a123f2",
                    "LayerId": "d5ca9079-e270-4001-9be2-1521e5c41886"
                }
            ]
        },
        {
            "id": "55321b5c-af78-497b-b278-724b5ac787b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35228085-dd2e-4625-9792-3beb4cead961",
            "compositeImage": {
                "id": "1027c255-6c7b-4d3f-9723-45f7b73c5465",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55321b5c-af78-497b-b278-724b5ac787b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "393025dc-7f2f-4c19-9919-0a87250eef81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55321b5c-af78-497b-b278-724b5ac787b8",
                    "LayerId": "d5ca9079-e270-4001-9be2-1521e5c41886"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d5ca9079-e270-4001-9be2-1521e5c41886",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35228085-dd2e-4625-9792-3beb4cead961",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 9,
    "yorig": 26
}