{
    "id": "adc848b5-29b6-42ea-8180-bd66deadf1c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprRedPiece",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 18,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d0717e0-302c-422b-947b-4602cc569d5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adc848b5-29b6-42ea-8180-bd66deadf1c1",
            "compositeImage": {
                "id": "1ccbdb51-ee44-4523-91dc-febfb0083947",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d0717e0-302c-422b-947b-4602cc569d5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4c3d222-6ac3-45bd-9c11-1a473be5004c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d0717e0-302c-422b-947b-4602cc569d5e",
                    "LayerId": "2c6d3736-506e-4259-9173-b38ed5bc1553"
                }
            ]
        },
        {
            "id": "1d456d81-3201-485f-9829-7432f8597207",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adc848b5-29b6-42ea-8180-bd66deadf1c1",
            "compositeImage": {
                "id": "61502a61-18a3-4da4-bc8a-3f6d2b340660",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d456d81-3201-485f-9829-7432f8597207",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42a7c9ec-c062-4776-ae81-d76b21bb9f49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d456d81-3201-485f-9829-7432f8597207",
                    "LayerId": "2c6d3736-506e-4259-9173-b38ed5bc1553"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2c6d3736-506e-4259-9173-b38ed5bc1553",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "adc848b5-29b6-42ea-8180-bd66deadf1c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 9,
    "yorig": 26
}